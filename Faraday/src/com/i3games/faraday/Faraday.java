/*This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/    	
    	
package com.i3games.faraday;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.puredata.android.io.AudioParameters;
import org.puredata.android.service.PdService;
import org.puredata.android.utils.PdUiDispatcher;
import org.puredata.core.PdBase;
import org.puredata.core.PdListener;
import org.puredata.core.utils.IoUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Resources.NotFoundException;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Vibrator;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Faraday extends Activity {

	class AboutDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(R.string.about_dialog)
					.setTitle(R.string.about_title)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});
			// Create the AlertDialog object and return it
			return builder.create();
		}
		@Override
		public void onStart() {
			super.onStart();
			TextView aboutText = (TextView) getDialog().findViewById(
					android.R.id.message);
			aboutText.setText(Html.fromHtml(getString(R.string.about_dialog)));
			aboutText.setMovementMethod(LinkMovementMethod.getInstance());
		}
	}

	class WifiReceiver extends BroadcastReceiver {
		public void onReceive(Context c, Intent intent) {
			int wifi = 0, sumLevels = 0;
			List<ScanResult> wifiList = mWifiManager.getScanResults();
			Log.d(TAG, "Receiving WIFI data");
			for (wifi = 0; wifi < wifiList.size(); wifi++) {

				// String ssid = wifiList.get(wifi).SSID;
				// String bssid = wifiList.get(wifi).BSSID;
				// int frequency = wifiList.get(wifi).frequency;
				int level = wifiList.get(wifi).level;

				// Log.d(TAG, "wifi " + i + ":" + ssid);
				// Log.d(TAG, "wifi " + i + ":" + bssid);
				// Log.d(TAG, "wifi " + i + ":" + frequency);
				// Log.d(TAG, "wifi " + i + ":" + level);

				sumLevels += level;
			}
			updateWifiDetector(wifi, sumLevels / wifi);
		}
	}

	public static final String TAG = "FARADAY";
	private static final String PATCH_FILE = "hum1.pd";
	private static final int VIBRATE_TIME = 1000;

	private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				Log.d(TAG, "Receiving Bluetooth data");
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				int rssi = Math.abs(intent.getShortExtra(
						BluetoothDevice.EXTRA_RSSI, (short) 0));
				updateBluetoothDetector(rssi);
			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
					.equals(action)) {
				// Scan again...
				mBluetoothAdapter.startDiscovery();
			}
		}
	};
	private BluetoothAdapter mBluetoothAdapter;
	private WifiManager mWifiManager;
	private WifiReceiver mWifiReceiver;
	private final ServiceConnection mPdConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "pd: service connected");
			mPdService = ((PdService.PdBinder) service).getService();
			initPd();
			loadPatch();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
		}
	};
	private PdService mPdService;
	private PdUiDispatcher mPdUIDispatcher;
	private int mPdPatchHandle = 0;

	private TextView mWifiText;
	private TextView mWifiSignalsText;
	private TextView mWifiValueText;
	private LocationManager mLocationManager;
	private TextView mSatText;
	private TextView mSatSignalsText;
	private TextView mSatValueText;
	private TextView mBTSignalsText;
	private TextView mBTValueText;
	private RelativeLayout mDebugView;
	private RelativeLayout mMainView;
	private TextView mScoreText;
	private Button mStartButton;
	private TextView mTimeText;
	private TextView mGameOverText;
	private TextView mFinalScoreText;
	private boolean mDebugVisible = false;

	private int mScore;
	private boolean mRunning;
	private int mTimeSeconds;
	private Vibrator mVibrator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faraday);
		mWifiText = (TextView) findViewById(R.id.textView7);
		mWifiSignalsText = (TextView) findViewById(R.id.textView8);
		mWifiValueText = (TextView) findViewById(R.id.textView9);
		mSatText = (TextView) findViewById(R.id.textView10);
		mSatSignalsText = (TextView) findViewById(R.id.textView11);
		mSatValueText = (TextView) findViewById(R.id.textView12);
		mBTSignalsText = (TextView) findViewById(R.id.textView15);
		mBTValueText = (TextView) findViewById(R.id.textView16);
		mDebugView = (RelativeLayout) findViewById(R.id.debugLayout);
		mMainView = (RelativeLayout) findViewById(R.id.mainLayout);
		mScoreText = (TextView) findViewById(R.id.textViewScoreValue);
		mTimeText = (TextView) findViewById(R.id.textViewTimeValue);
		mGameOverText = (TextView) findViewById(R.id.textViewGameOver);
		mFinalScoreText = (TextView) findViewById(R.id.textViewFinalScore);

		mStartButton = (Button) findViewById(R.id.button1);
		mStartButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				reset();
				mRunning = true;
				mStartButton.setEnabled(false);
				mGameOverText.setVisibility(View.INVISIBLE);
				mFinalScoreText.setVisibility(View.INVISIBLE);

				new CountDownTimer(mTimeSeconds * 1000, 1000) {


					public void onTick(long millisUntilFinished) {
						mTimeText.setText("" + millisUntilFinished / 1000);
					}

					public void onFinish() {
						mVibrator.vibrate(VIBRATE_TIME);
						mTimeText.setText("done!");
						mFinalScoreText.setText("" + mScore);
						mGameOverText.setVisibility(View.VISIBLE);
						mFinalScoreText.setVisibility(View.VISIBLE);
						mStartButton.setEnabled(true);
						mRunning = false;
					}
				}.start();

			}
		});
		initPdService();
		init();
	}

	private void init() {
		reset();
		mTimeSeconds = 120;

		mWifiManager = (WifiManager) getApplicationContext().getSystemService(
				Context.WIFI_SERVICE);
		mWifiReceiver = new WifiReceiver();
		registerReceiver(mWifiReceiver, new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		boolean scanStarted = mWifiManager.startScan();
		Log.d(TAG, "scanStarted: " + scanStarted);

		mLocationManager = (LocationManager) getApplicationContext()
				.getSystemService(Context.LOCATION_SERVICE);
		mLocationManager.addGpsStatusListener(new Listener() {

			@Override
			public void onGpsStatusChanged(int event) {
				Log.d(TAG, "Receiving Satellite data");
				GpsStatus status = mLocationManager.getGpsStatus(null);
				Iterable<GpsSatellite> satellites = status.getSatellites();
				int size = 0;
				float sumSnrs = 0;
				for (GpsSatellite sat : satellites) {
					size++;
					sumSnrs += sat.getSnr();
				}
				updateSatDetector(size, sumSnrs / size);
			}
		});
		mLocationManager.requestLocationUpdates("gps", 500, 0,
				new LocationListener() {

					@Override
					public void onStatusChanged(String provider, int status,
							Bundle extras) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProviderEnabled(String provider) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProviderDisabled(String provider) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onLocationChanged(Location location) {
						// TODO Auto-generated method stub

					}
				});
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		registerReceiver(mBluetoothReceiver, filter);
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				mBluetoothAdapter.startDiscovery();

			}
		}, 0, 2000);
		mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	}

	private void reset() {
		mRunning = false;
		mScore = 0;
	}

	@Override
	protected void onDestroy() {
		if (mWifiReceiver != null)
			unregisterReceiver(mWifiReceiver);
		if (mBluetoothReceiver != null)
			unregisterReceiver(mBluetoothReceiver);
		closePatch();
		if (mPdService.isRunning())
			mPdService.stopAudio();
		mPdUIDispatcher.release();
		mVibrator.cancel();
		try {
			unbindService(mPdConnection);
		} catch (Exception e) {
			Log.e(TAG, "onDestroy() Exception");
		}
		super.onDestroy();
	}

	private void initPdService() {
		new Thread() {

			@Override
			public void run() {
				Log.d(TAG, "initPdService");
				bindService(new Intent(Faraday.this, PdService.class),
						mPdConnection, BIND_AUTO_CREATE);
			}
		}.start();
	}

	private void loadPatch() {
		if (mPdPatchHandle == 0) {
			File dir = getFilesDir();
			try {
				IoUtils.extractZipResource(
						getResources().openRawResource(
								com.i3games.faraday.R.raw.patch), dir, true);
				File patchFile = new File(dir, PATCH_FILE);
				mPdPatchHandle = PdBase.openPatch(patchFile.getAbsolutePath());
				Log.d(TAG, "opening patch: " + PATCH_FILE);
			} catch (NotFoundException e) {
				Log.e(TAG, "loadPatch pd bombed " + e.toString());
				finish();
			} catch (IOException e) {
				Log.e(TAG, "loadPatch pd bombed " + e.toString());
				finish();
			}
			initPatch();
		}

	}

	private void initPatch() {
		// TODO Auto-generated method stub
	}

	private void initPd() {
		int sampleRate = AudioParameters.suggestSampleRate();
		Log.d(TAG, "pd: suggested samplerate: " + sampleRate);
		sampleRate = sampleRate / 2;
		try {
			mPdService.initAudio(sampleRate, 0, 2, 10.0f);
		} catch (IOException e) {
			Log.e(TAG, "init pd bombed " + e.toString());
			finish();
		}
		mPdService.startAudio(new Intent(this, Faraday.class), R.drawable.icon,
				getResources().getString(R.string.app_name), getResources()
						.getString(R.string.return_message));
		mPdUIDispatcher = new PdUiDispatcher();
		PdBase.setReceiver(mPdUIDispatcher);
		mPdUIDispatcher.addListener("x", new PdListener.Adapter() {
			@Override
			public void receiveFloat(String source, final float x) {

			}
		});

	}

	protected void closePatch() {
		PdBase.closePatch(mPdPatchHandle);
	}

	public void updateWifiDetector(int wifis, int strength) {

		int value = Math.abs(wifis * strength);
		Log.d(TAG, "wifis " + wifis + ", levels: " + strength + ", value: "
				+ value);
		if (mDebugVisible) {
			mWifiText.setText("" + wifis);
			mWifiSignalsText.setText("" + strength);
			mWifiValueText.setText("" + value);
		}
		addScore(value);
		sendToPatch("WIFI", value);
	}

	private void addScore(int value) {
		if (mRunning) {
			mScore += value;
			mScoreText.setText("" + mScore);
		}
	}

	public void updateSatDetector(int sats, float strength) {

		int value = (int) Math.abs(sats * strength);
		Log.d(TAG, "sats " + sats + ", levels: " + strength + ", value: "
				+ value);
		if (mDebugVisible) {
			mSatText.setText("" + sats);
			mSatSignalsText.setText("" + strength);
			mSatValueText.setText("" + value);
		}
		addScore(value);
		sendToPatch("SATS", value);
	}

	public void updateBluetoothDetector(float strength) {

		Log.d(TAG, "bluetooth rssi " + strength);
		int value = (int) ((float) strength * 2.5f);
		if (mDebugVisible) {
			mBTSignalsText.setText("" + strength);
			mBTValueText.setText("" + value);
		}
		addScore(value);
		sendToPatch("BT", value);

	}

	private void sendToPatch(String tag, int value) {
		if (tag == "WIFI") {
			PdBase.sendFloat("pd_wifi", value);
		} else if (tag == "SATS") {
			Log.d(TAG, "sending SATS value: " + value);
			PdBase.sendFloat("pd_sats", value);
		} else if (tag == "BT") {
			Log.d(TAG, "sending BLUETOOTH value: " + value);
			PdBase.sendFloat("pd_bt", value);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hayday, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.menu_about) {
			new AboutDialogFragment().show(getFragmentManager(), "about");
			return true;
		}
		if (id == R.id.menu_debug) {
			if (mDebugVisible) {
				mMainView.setVisibility(View.VISIBLE);
				mDebugView.setVisibility(View.INVISIBLE);
			} else {
				mDebugView.setVisibility(View.VISIBLE);
				mMainView.setVisibility(View.INVISIBLE);
			}
			mDebugVisible = !mDebugVisible;
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
