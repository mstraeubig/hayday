## Every Dog has its Faraday

You are Faraday. You were dog-napped from your litter as a puppy and dumped in a research laboratory running a secret military program to develop super-intelligent dogs that are sensitised to digital signals. After months in the program, you are smart, aware and terribly lonely. You miss your brothers and sisters and you desperately want to return home. You inherently mistrust humans, your two-legged jailers.

One day, a thoughtless lab assistant leaves you with an opportunity to escape. You know you are tagged and being tracked and the lab will be desperate to recapture you.

You must make your way home, avoiding capture by making yourself as hard as possible to detect by the lab's digital tracking technology. To do so, you must avoid, as best you can, all kinds of radio signals coming from wifi, bluetooth and GPS satellites.

Made for the Mixed Reality Game Design workshop organized by the Performance and Games Network and took place at the The Mixed Reality Laboratory (MRL), University of Nottingham. [http://performance-games.lincoln.ac.uk/workshopnottingham-october-2014/]

Code by Michael Straeubig.
Bluetooth code by James Munro.
Concept / Game Design by Kate Eltham, James Munro, Patrcick Dickinson, Michael Straeubig, Gavin Wood.
Back story by Kate Eltham.
Ideation framework and Game Design by Richard Wetzel.

Libpd by Peter Brinkmann and others.
Pure Data by Miller Puckette and others.
Pure Data patch by Andy Farnell [http://mitpress.mit.edu/books/designing-sound]

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.